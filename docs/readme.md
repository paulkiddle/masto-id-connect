# Masto ID Connect

Why maintain your own auth system when you can use someone else's?

## Issuer discovery, client registration, and user authentication methods for Mastodon

This library provides a simple set of functions for authenticating a user against a Mastodon API. It only provides methods relevant to authentication - server discovery, client registration, token exchange, and user info fetching.

The methods provided are similar to those that you might find in a general OIDC client library. Mastodon does not currently support OIDC, but [a feature request has been made](https://github.com/tootsuite/mastodon/issues/4800). In the meantime you can use this library to achieve the same goal using Mastodon's proprietray API.

## Usage

```javascript
import http from 'http';
import mastoIdConnect, { OOB } from 'masto-id-connect';

const masto = mastoIdConnect();

// First we must discover the issuer and register our client.
// You will only need to do these once - here we're doing it
// at the start of our script, but in reality you may want to
// do it dynamically when a user inputs their mastodon URL.
// In that case you'll need to store the results in a database
// so you can look them up again later.
const kithKitchen = await masto.discoverIssuer('https://kith.kitchen');
const kkClient = await masto.registerClient(kithKitchen, {
	clientName: 'Example Client',
	// The redirect URI to return the user to your webserver after they log in to mastodon.
	// You can also use the out-of-band approach by passing the OOB const
	redirectUri: 'http://localhost:8080/exchange-token'
});

// This is an example webserver - obviously yours will be different but the principle is the same
http.createServer(async (req, res) => {
	const url = new URL(req.url, 'http://localhost:8080');
	switch(url.pathname) {
		case '/authenticate':
			// This is our user's entry point - direct the user to the mastodon auth page.
			// Here we have hardcoded kith.kitchen, but you could use the discover and register
			// methods to allow dynamic authentication against any instance the user wants.
			const kkUrl = masto.getAuthUrl(kithKitchen, kkClient);
			res.statusCode = 303;
			res.setHeader('Location', kkUrl);
			res.end();
			return;
		case '/exchange-token':
			// Get the issuer and the authentication code from the request
			// If you're using the out-of-band methdod, you don't need this step
			// since the user will paste the code directly into your app.
			const { issuer, code } = masto.parseResponse(req);

			// Here we check that the issuer is our hardcoded instance
			// But you could use it to look up your issuer and client details
			// from a database if you've stored them there.
			if(issuer !== 'https://kith.kitchen') {
				res.statusCode = 500;
				res.end('Cannot authenticate against that instance');
				return;
			}

			// Next we get our authentication token
			const token = await masto.exchangeToken(kithKitchen, kkClient, code);

			// You could use this token to browse the mastodon API now - see the masto docs for more
			// Instead we are going to get the user's info. This is enough to log the user into our website
			const userInfo = await masto.getUserInfo(kithKitchen, token);

			// For more about the userInfo object, see [the open ID spec](https://openid.net/specs/openid-connect-core-1_0.html#StandardClaims)
			res.end(`You are ${userInfo.name} and your URL is ${userInfo.sub}`);
			return;
	}
}).listen(8080);

```

import lib, {OOB, MastoError} from '../src/index.js';
import {jest} from '@jest/globals';

const mockRequest = body => {
	const request = jest.fn(url => ({
		on(ev, cb){
			cb([body instanceof Function ? body(url) : body]);
		},
		end: request.end
	}));

	request.end = jest.fn(function(){
		return this;
	});

	return request;
};

test('Discovers issuer', async ()=>{
	const request = mockRequest(JSON.stringify({uri: 'example.domain'}));

	const ma = lib({
		request
	});
	expect(await ma.discoverIssuer('https://example.domain')).toMatchSnapshot();
	expect(request).toHaveBeenCalledWith('https://example.domain/api/v1/instance');
});

test('Discovers issuer from domain only', async ()=>{
	const request = mockRequest(JSON.stringify({uri: 'example.domain'}));

	const ma = lib({
		request
	});
	expect(await ma.discoverIssuer('example.domain')).toMatchSnapshot();
	expect(request).toHaveBeenCalledWith('https://example.domain/api/v1/instance');
});

test('Registers client', async ()=>{
	const request = mockRequest(JSON.stringify({client_secret: 'client_secret'}));

	const issuer = {
		issuer: 'https://example.domain'
	};

	const options = {
		redirectUri: 'http://local.example/exchange-token',
		clientName: 'Test Client'
	};

	const ma = lib({
		request
	});

	expect(await ma.registerClient(issuer, options)).toMatchSnapshot();
	expect(request.end.mock.calls).toMatchSnapshot();
});

test('Registers with OOB auth URL', async ()=>{
	const request = mockRequest(JSON.stringify({client_secret: 'client_secret'}));

	const issuer = {
		issuer: 'https://example.domain'
	};

	const options = {
		redirectUri: OOB,
		clientName: 'Test Client'
	};

	const ma = lib({
		request
	});

	expect(await ma.registerClient(issuer, options)).toMatchSnapshot();
	expect(request.end.mock.calls).toMatchSnapshot();
});

test('Gets auth URL', ()=> {
	const issuer = {
		issuer: 'https://example.domain',
		authorization_endpoint: 'https://example.domain/oauth/authorization'
	};
	const client = {
		client_id: 12345,
		redirect_uri: 'http://localhost/exchange-token'
	};
	const ma = lib();
	expect(ma.getAuthUrl(issuer, client)).toMatchSnapshot();
});

test('Parses HTTP response', () => {
	const ma = lib();

	const req = {
		headers: {
			host: 'example.com'
		},
		url: '/callback?code=my-code&issuer=example.host'
	};

	expect(ma.parseResponse(req)).toMatchSnapshot();
});

test('Exchange token', async () => {
	const issuer = {
		token_endpoint: 'http://example.domain/oauth/token'
	};
	const client = {
		client_id: 'client-id',
		client_secret: 'client-secret',
		redirect_uri: 'redirect-uri'
	};
	const code = 'my-code';

	const request = mockRequest('{"token":"my-token"}');
	const ma = lib({ request });
	expect(await ma.exchangeToken(issuer, client, code)).toMatchSnapshot();
	expect(request.mock.calls).toMatchSnapshot();
	expect(request.end.mock.calls).toMatchSnapshot();
});


test('Exchange token error', async () => {
	const issuer = {
		token_endpoint: 'http://example.domain/oauth/token'
	};
	const client = {
		client_id: 'client-id',
		client_secret: 'client-secret',
		redirect_uri: 'redirect-uri'
	};
	const code = 'my-code';

	const request = mockRequest('{"error":"bad_event", "error_description": "A bad event occured" }');
	const ma = lib({ request });
	let p;
	await expect(p=ma.exchangeToken(issuer, client, code)).rejects.toBeInstanceOf(MastoError);
	expect(await p.catch(e=>e)).toMatchSnapshot();
});

test('Get user info', async() => {
	const issuer = {
		token_endpoint: 'https://example.host/token-exchange'
	};
	const token = {
		token_type: 'Bearer',
		access_token: 'my-token'
	};
	const userInfo = {
		url: 'https://example.host/@user',
		display_name: 'Dr. User'
	};

	const request = mockRequest(url => {
		switch(url) {
			case 'https://example.host/api/v1/accounts/verify_credentials':
				return JSON.stringify(userInfo);
			case userInfo.url:
				return JSON.stringify({
					id: userInfo.url,
					preferredUsername: 'User',
					icon: {
						url: 'http://example.com/img.jpg'
					}
				});
		}
	});
	const ma = lib({request});
	expect(await ma.getUserInfo(issuer, token)).toMatchSnapshot();
});

test('Get user info - errors if user and auth urls have different hosts', async() => {
	const issuer = {
		token_endpoint: 'https://example.host/token-exchange'
	};
	const token = {
		token_type: 'Bearer',
		access_token: 'my-token'
	};
	const userInfo = {
		url: 'https://different.example.host/@user',
		display_name: 'Dr. User'
	};

	const request = mockRequest(JSON.stringify(userInfo));
	const ma = lib({request});
	await expect(ma.getUserInfo(issuer, token)).rejects.toThrow(Error);
});

test('Get user info - errors if user profile URL has a different domain', async() => {
	const issuer = {
		token_endpoint: 'https://example.host/token-exchange'
	};
	const token = {
		token_type: 'Bearer',
		access_token: 'my-token'
	};
	const userInfo = {
		url: 'https://example.host/@user',
		display_name: 'Dr. User'
	};

	const request = mockRequest(url => {
		switch(url) {
			case 'https://example.host/api/v1/accounts/verify_credentials':
				return JSON.stringify(userInfo);
			case userInfo.url:
				return JSON.stringify({
					id: 'http://different.example.host/@User',
					preferredUsername: 'User',
					icon: {
						url: 'http://example.com/img.jpg'
					}
				});
		}
	});
	const ma = lib({request});
	await expect(ma.getUserInfo(issuer, token)).rejects.toThrow(Error);
});

import https from 'https';

/**
@module mastoIdConnect
*/

/**
 * Class representing a Mastodon API error
 * @param {String} code The mastodon api error code
 */
export class MastoError extends Error {
	constructor({ error, error_description }) {
		super(error_description);
		this.code = error;
	}
}

// Todo: Add specific types: invalid_grant, invalid_scope, invalid_client
// add masto error on other function calls?

/**
 * Out of band URN - use this as the redirect_uri for non-browser-based applications
 */
export const OOB = 'urn:ietf:wg:oauth:2.0:oob';

// Private - get the response of an http request
function getResponse(req) {
	return new Promise((resolve, reject) => {
		req.on('response', resolve);
		req.on('error', reject);
	});
}

// Private - get the body of a stream
async function getBody(res) {
	let body = '';
	for await(const data of res) {
		body+= data;
	}
	return body;
}

// Private - perform a GET request on a JSON document
async function getJson(request, ...opts) {
	const req = request(...opts).end();
	const res = await getResponse(req);
	const body = await getBody(res);
	return JSON.parse(body);
}

/**
 * Create an instance of the Mastodon auth library
 * @alias module:mastoIdConnect.default
 * @param {Function} options.request Method for making https requests, having the same signature as https.request (which is the default value)
 * @param {string} protocol Protocol to use when making http requests, including '://'. Defaults to 'https://'
 * @param {string} issuerKey Key to use as the issuer identifier in the redirect URL's search params. Defaults to 'issuer
 */
export default ({ request = https.request, protocol = 'https://', issuerKey = 'issuer' }={}) => ({
	/**
	 * Get document describing oauth endpoints for the Mastodon instance
	 * This is not strictly needed but it helps if you don't know whether your url has a masto API or not
	 * @param	{string} Origin The url or domain name for the instance
	 * @returns {object} An object with `issuer`, `authorization_endpoint` and `token_endpoint` urls
	 */
	async discoverIssuer(origin){
		if(origin.indexOf(protocol) !== 0) {
			origin = protocol + origin;
		}
		const url = new URL('/api/v1/instance', origin).toString();
		const resPayload = await getJson(request, url);
		const issuer = protocol + resPayload.uri;

		// Present something similar to openid connect discovery document
		// https://openid.net/specs/openid-connect-discovery-1_0.html
		return {
			issuer,
			authorization_endpoint: issuer + '/oauth/authorize',
			token_endpoint: issuer + '/oauth/token',
			// could add in other endpoints if they were supported
			/*jwks_uri: <string>
			userinfo_endpoint: <string>
			revocation_endpoint: <string>
			introspection_endpoint: <string>
			end_session_endpoint: <string>
			registration_endpoint: <string>
			token_endpoint_auth_methods_supported: <string>
			token_endpoint_auth_signing_alg_values_supported: <string>
			introspection_endpoint_auth_methods_supported: <string>
			introspection_endpoint_auth_signing_alg_values_supported: <string>
			revocation_endpoint_auth_methods_supported: <string>
			revocation_endpoint_auth_signing_alg_values_supported: <string>
			request_object_signing_alg_values_supported: <string>
			mtls_endpoint_aliases: <Object>
				token_endpoint: <string>
				userinfo_endpoint: <string>
				revocation_endpoint: <string>
				introspection_endpoint: <string>
				*/
		};
	},

	/**
	 * Register your client with the Mastodon instance
	 * @param {Object} issuer Issuer object returned by discoverIssuer
	 * @param {Object} options Client options containing redirectUri and clientName
	 * @returns {Object} Client object
	 */
	async registerClient(issuer, options) {
		const endpoint = new URL('/api/v1/apps/', issuer.issuer);
		let redirectUri;

		if(options.redirectUri === OOB){
			redirectUri = OOB;
		} else {
			const u = new URL(options.redirectUri);
			u.searchParams.append(issuerKey, endpoint.host);
			redirectUri = u.toString();
		}

		const params = new URLSearchParams({
			client_name: options.clientName,
			redirect_uris: [
				redirectUri
			],
			//scopes,
			//website
		});

		const req = request(endpoint, { method: 'POST' });
		req.end(params.toString());
		const res = await getResponse(req);

		// if(res.statusCode === 301) {
		// 	return register(res.headers.location);
		// }

		const body = await getBody(res);

		return JSON.parse(body);
	},

	/**
	 * Generate the URL to redirect the user to in order to authorize them
	 * @param {Object} issuer Object returned by discoverIssuer
	 * @param {Object} client Object returned by registerClient
	 * @return {string} URL to redirec the user to
	 */
	getAuthUrl(issuer, client) {
		const params = '?' + new URLSearchParams({
			scope: 'read',
			client_id: client.client_id,
			redirect_uri: client.redirect_uri,
			response_type: 'code'
		});
		return issuer.authorization_endpoint + params;
	},

	/**
	 * Get the issuer and code from an incomming request.
	 * Based on requests coming in when using http(s).createServer
	 * This step is not required if you're using the OOB URN
	 * @param {https.IncommingMessage} req The client request object received by the server
	 */
	parseResponse(req) {
		const url = new URL(protocol + req.headers.host + req.url);
		return {
			code: url.searchParams.get('code'),
			issuer: url.searchParams.get(issuerKey)
		};
	},

	/**
	 * Exchange your auth code for an auth token. The auth token is what lets you actually make requests.
	 * @param {Object} issuer Object returned by discoverIssuer
	 * @param {Object} client Object returned by registerClient
	 * @param {string} code Code returned by parseResponse (or provided by user input if using OOB URN)
	 * @returns {Object} Object containing access_token and token_type
	 * @throws {MastoError} When an error is returned by the oauth endpoint
	 */
	async exchangeToken(issuer, client, code) {
		//const uri = client.redirect_uris[0];
		const tokenEndpoint = issuer.token_endpoint;
		const req = request(
			tokenEndpoint,
			{
				method: 'POST',
				headers: {
					'Content-Type': 'application/json'
				}
			}
		);
		req.end(JSON.stringify({
			client_id: client.client_id,
			client_secret: client.client_secret,
			redirect_uri: client.redirect_uri,
			code: code,
			grant_type: 'authorization_code'
		}));
		const res = await getResponse(req);
		const body = await getBody(res);
		const parsed = JSON.parse(body);

		if(parsed.error) {
			throw new MastoError(parsed);
		}

		return parsed;
	},

	/**
	 * Information about the user, structured as an OIDC claims document
	 * @typedef {Object} UserInfo
	 * @see {@link https://openid.net/specs/openid-connect-core-1_0.html#StandardClaims|OIDC standard claims documentation}
	 * @property {string} sub The ID/URL of the actor (subject)
	 * @property {string} name The display name of the actor
	 * @property {string} nickname Alias of name
	 * @property {string} preferred_username The actor's preferredUsername
	 * @property {string} profile Alias of sub
	 * @property {string} picture The URL of the actor's avatar (icon.url)
	 */

	/**
	 * Get information about the authenticated user.
	 * Presents information in the same format as if you were accessing an OIDC user info endpoint.
	 * For more information see [the open ID spec](https://openid.net/specs/openid-connect-core-1_0.html#StandardClaims)
	 * @param {Object} issuer Object returned by discoverIssuer
	 * @param {Object} token Object returned by exchangeToken
	 * @returns {UserInfo} Userinfo object
	 */
	async getUserInfo(issuer, token) {
		const verifyUrl = new URL('/api/v1/accounts/verify_credentials', issuer.token_endpoint);
		const { url, display_name } = await getJson(request,
			verifyUrl.toString(),
			{
				headers: {
					Authorization: token.token_type + ' ' + token.access_token
				}
			}
		);

		if(new URL(url).host !== verifyUrl.host) {
			throw new Error(`User url (${url}) is not at the same origin as auth endpoint (${verifyUrl.host})`);
		}

		const name = display_name;

		const { id, preferredUsername, icon } = await getJson(request,
			url,
			{
				headers: {
					Accept: 'application/json',
					Authorization: token.token_type + ' ' + token.access_token
				}
			}
		);


		if(new URL(id).host !== verifyUrl.host) {
			throw new Error(`User id (${id}) is not at the same origin as auth endpoint (${verifyUrl.host})`);
		}

		// https://openid.net/specs/openid-connect-core-1_0.html#StandardClaims
		return { sub:id, name, nickname: name, preferred_username: preferredUsername, profile:id, picture: icon.url };
	}
});
